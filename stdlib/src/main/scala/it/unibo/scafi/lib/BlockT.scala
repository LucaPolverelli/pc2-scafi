/*
 * Copyright (C) 2016-2017, Roberto Casadei, Mirko Viroli, and contributors.
 * See the LICENCE.txt file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

package it.unibo.scafi.lib

import scala.concurrent.duration.Duration

trait Stdlib_BlockT {
  self: StandardLibrary.Subcomponent =>

  // scalastyle:off method.name

  trait BlockT extends FieldCalculusSyntax {

    def T[V:Numeric](initial: V, floor: V, decay: V => V): V = {
      rep(initial) { v =>
        implicitly[Numeric[V]].min(initial, implicitly[Numeric[V]].max(floor, decay(v)))
      }
    }

    def timer(length: Double) = T[Double](length,0,_-1.0)

    def limitedMemory[V](value: V, expValue: V, timeout: Double): (V, Double) = {
      (value,timer(timeout)) match {case (v,t) if (t<=0) => (expValue,t); case r=>r}
    }
  }
}
